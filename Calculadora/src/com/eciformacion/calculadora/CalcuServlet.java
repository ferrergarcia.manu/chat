package com.eciformacion.calculadora;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CalcuServlet
 */
@WebServlet("/calcular")
public class CalcuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public CalcuServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Calculadora c = new Calculadora(Integer.parseInt(request.getParameter("primer")), Integer.parseInt(request.getParameter("segun")));
		switch (request.getParameter("operar")) {
		case "suma":
			response.getWriter().write("<p>Resultado = "+c.sumar()+"</p>");
			break;
		case "resta":
			response.getWriter().write("<p>Resultado = "+c.restar()+"</p>");
			break;
		case "multi":
			response.getWriter().write("<p>Resultado = "+c.multi()+"</p>");
			break;
		case "dividir":
			response.getWriter().write("<p>Resultado = "+c.divi()+"</p>");
			break;
		default:
			break;
		}
		PrintWriter out= response.getWriter();
		out.println("<a href=\"calculadora.html\">hi</a>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
