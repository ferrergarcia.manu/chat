package com.eciformacion.calculadora;

public class Calculadora {
	private int primer;	
	private int segun;
	public Calculadora(int primer, int segun) {
		super();
		this.primer = primer;
		this.segun = segun;
	}
	public int sumar() {
		return this.primer+this.segun;
	}
	public int restar() {
		return this.primer-this.segun;
	}
	public double multi() {
		return this.primer*this.segun;
	}
	public double divi() {
		return this.primer/this.segun;
	}
	public int getPrimer() {
		return primer;
	}
	public void setPrimer(int primer) {
		this.primer = primer;
	}
	public int getSegun() {
		return segun;
	}
	public void setSegun(int segun) {
		this.segun = segun;
	}
}
