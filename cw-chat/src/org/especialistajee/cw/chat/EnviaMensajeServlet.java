package org.especialistajee.cw.chat;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import org.especialistajee.cw.chat.to.ColaMensajes;
import org.especialistajee.cw.chat.to.Mensaje;

import java.io.*;

@WebServlet("/EnviaMensajeServlet")
public class EnviaMensajeServlet extends HttpServlet {

	private static final long serialVersionUID = -481257371131179080L;
	private ServletContext ctx;

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		ctx = config.getServletContext();
		super.init(config);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		RequestDispatcher rd ;
		Object nick = req.getSession().getAttribute("org.especialistajee.cw.chat.nick");
		// TODO: Si no hay usuario, redireccionar a /chat/error.html
		if(req.getSession().getAttribute("org.especialistajee.cw.chat.nick").equals(null)) {
			System.out.println("no hay sesion");
			rd = this.ctx.getRequestDispatcher("/chat/error.html");
			//rd.forward(req, res);
		}else {
		// TODO: Agregar mensaje enviado a la cola de mensajes
			String cadena = req.getParameter("texto");
			System.out.println(nick+": "+cadena);
			ColaMensajes c = (ColaMensajes)ctx.getAttribute("colaMensajes");
			c.addMessage(new Mensaje((String)nick, cadena));
			rd = this.ctx.getRequestDispatcher("/chat/enviaMensaje.html");
			//rd.include(req, res);
		// TODO: Redireccionar a /chat/enviaMensaje.html
		}
		rd.forward(req, res);
		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		doGet(req,res);
	}
}
