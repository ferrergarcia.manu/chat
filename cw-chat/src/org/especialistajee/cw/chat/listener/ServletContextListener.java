package org.especialistajee.cw.chat.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
//import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.especialistajee.cw.chat.to.ColaMensajes;

/**
 * Application Lifecycle Listener implementation class ServletContextListener
 *
 */
@WebListener
public class ServletContextListener implements javax.servlet.ServletContextListener {

    /**
     * Default constructor. 
     */
    public ServletContextListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    	ColaMensajes c = new ColaMensajes();
    	arg0.getServletContext().setAttribute("colaMensajes", c);
    }
	
}
