package org.especialistajee.cw.chat;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import org.especialistajee.cw.chat.to.ColaMensajes;
import org.especialistajee.cw.chat.to.Mensaje;

import java.io.*;

@WebServlet("/ListaMensajesServlet")
public class ListaMensajesServlet extends HttpServlet {

	private static final long serialVersionUID = 427199619647569137L;
	private ServletContext ctx;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		ctx = config.getServletContext();
		super.init(config);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException		
	{
		
		PrintWriter op = res.getWriter();
		ColaMensajes c = (ColaMensajes) ctx.getAttribute("colaMensajes");
		// TODO: Añadir cabecera para actualizar cada 5 segundos
		res.addIntHeader("Refresh", 5);
		// TODO: Incluir /chat/cabecera.htmlf
		RequestDispatcher rd = ctx.getRequestDispatcher("/chat/cabecera.htmlf");
		rd.include(req, res);
		// TODO: Mostrar mensajes del chat
		for (Mensaje mensaje : c) {
			op.println("<p>"+mensaje.getEmisor()+": "+mensaje.getTexto()+"</p>");
		}
		// TODO: Incluir /chat/pie.htmlf
		rd = ctx.getRequestDispatcher("/chat/pie.htmlf");
		rd.include(req, res);
		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException
	{
		doGet(req,res);
	}
}
